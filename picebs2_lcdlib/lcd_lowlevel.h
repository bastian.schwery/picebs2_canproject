#include <pic18.h>
#include <stdint.h>


#define _XTAL_FREQ   64000000L

/******************************************************************************/
/* Display selection                                                          */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/* choose one of the two choices                                              */
/******************************************************************************/
#define  DISPLAY_NHD 24   // 2.4 " display (controller ILI9163)
//#define  DISPLAY_NHD   18   // 1.8 " display (controller ILI9341)
/******************************************************************************/
/* Display direction choice                                                   */
/* todo - > vertical modes have not been tested !!!                           */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/* choose one of the four choices                                             */
/* the normal direction means the connector is to the left (or to the top)    */
/******************************************************************************/
//#define HORIZONTAL_NORMAL
#define HORIZONTAL_INVERTED
//#define VERTICAL_NORMAL
//#define VERTICAL_INVERTED

#ifdef HORIZONTAL_NORMAL
#define CMD_MEM_ACC_DATA     0b01101000
#define CMD_MEM_ACC_DATA_BMP 0b00101000
#if DISPLAY_NHD == 24
#define   LCD_WIDTH  320
#define   LCD_HEIGHT 240
#endif
#if DISPLAY_NHD == 18
#define   LCD_WIDTH  160
#define   LCD_HEIGHT 128
#endif
#endif
#ifdef HORIZONTAL_INVERTED
#define CMD_MEM_ACC_DATA     0b10101000
#define CMD_MEM_ACC_DATA_BMP 0b11101000
#if DISPLAY_NHD == 24
#define   LCD_WIDTH  320
#define   LCD_HEIGHT 240
#endif
#if DISPLAY_NHD == 18
#define   LCD_WIDTH  160
#define   LCD_HEIGHT 128
#endif
#endif
#ifdef VERTICAL_NORMAL
#define CMD_MEM_ACC_DATA     0b11001000
#define CMD_MEM_ACC_DATA_BMP 0b01001000
#if DISPLAY_NHD == 24
#define   LCD_WIDTH  240
#define   LCD_HEIGHT 320
#endif
#if DISPLAY_NHD == 18
#define   LCD_WIDTH  128
#define   LCD_HEIGHT 160
#endif
#endif
#ifdef VERTICAL_INVERTED
#define CMD_MEM_ACC_DATA     0b00001000
#define CMD_MEM_ACC_DATA_BMP 0b10001000
#if DISPLAY_NHD == 24
#define   LCD_WIDTH  240
#define   LCD_HEIGHT 320
#endif
#if DISPLAY_NHD == 18
#define   LCD_WIDTH  128
#define   LCD_HEIGHT 160
#endif
#endif
/******************************************************************************/
/* constantes	for ILI9341 commands access                                     */
/******************************************************************************/
#if DISPLAY_NHD == 24

#define     CMD_SOFTRESET       0x01
#define     CMD_DISPOFF         0x28
#define     CMD_DISPON          0x29
#define     CMD_SLPOUT          0x11
#define     CMD_PWR_CTRL_A      0xCB
#define     CMD_PWR_CTRL_B      0xCF
#define     CMD_DRV_TIM_CTRL_A  0xE8
#define     CMD_DRV_TIM_CTRL_B  0xEA
#define     CMD_PWR_ON_SEQ_CTRL 0xED
#define     CMD_PUMP_RATIO_CTRL 0xF7
#define     CMD_INTERFACE_CTRL  0xF6
#define     CMD_PWR_CTRL_1      0xC0
#define     CMD_PWR_CTRL_2      0xC1
#define     CMD_VCOM_CTRL_1     0xC5
#define     CMD_VCOM_CTRL_2     0xC7
#define     CMD_MEM_ACC_CTRL    0x36
#define     CMD_FRAME_RATE_CTRL 0xB1
#define     CMD_DISP_FUNC_CTRL  0xB6
#define     CMD_PIXEL_FORMAT    0x3A
#define     GAMMA_CONTROL       0xF2
#define     GAMMA_CURVE         0x26
#define     COLUMN_ADDRESS      0x2A
#define     PAGE_ADDRESS        0x2B
#define     MEMORY_WRITE        0x2C
#define     MEMORY_READ         0x2E
/******************************************************************************/
/* constantes	for ILI9163 commands access                                     */
/******************************************************************************/
#elif DISPLAY_NHD == 18
#define     CMD_SOFTRESET       0x01
#define     GAMMA_CURVE         0x26
#define     GAMMA_CONTROL       0xF2
#define     CMD_FRAME_RATE_CTRL 0xB1
#define     CMD_PWR_CTRL_1      0xC0
#define     CMD_PWR_CTRL_2      0xC1
#define     CMD_VCOM_CTRL_1     0xC5
#define     CMD_VCOM_CTRL_2     0xC7
#define     COLUMN_ADDRESS      0x2A
#define     PAGE_ADDRESS        0x2B
#define     CMD_MEM_ACC_CTRL    0x36
#define     CMD_PIXEL_FORMAT    0x3A
#define     CMD_DISPON          0x29
#define     MEMORY_WRITE        0x2C


#define     CMD_DISPOFF         0x28
#define     CMD_SLPOUT          0x11
#define     CMD_PWR_CTRL_A      0xCB
#define     CMD_PWR_CTRL_B      0xCF
#define     CMD_DRV_TIM_CTRL_A  0xE8
#define     CMD_DRV_TIM_CTRL_B  0xEA
#define     CMD_PWR_ON_SEQ_CTRL 0xED
#define     CMD_PUMP_RATIO_CTRL 0xF7
#define     CMD_INTERFACE_CTRL  0xF6
#define     MEMORY_READ         0x2E

#endif

//------------------------------------------------------------------------------
// LCD connected pins definitions
//------------------------------------------------------------------------------
#define LCD_nRD           LATE0
#define LCD_nWR           LATE1
#define LCD_DnC           LATE3
#define LCD_nRES          LATE4
#define LCD_nCS           LATE2

#define DIR_LCD_nRD       TRISE0
#define DIR_LCD_nWR       TRISE1
#define DIR_LCD_DnC       TRISE3
#define DIR_LCD_nRES      TRISE4
#define DIR_LCD_nCS       TRISE2

#define LCD_DATA_BUS      PORTJ
#define DIR_LCD_DATA_BUS  TRISJ

#define LCD_BACKLIGHT_DIR   TRISE5
#define LCD_BACKLIGHT       LATE5

//------------------------------------------------------------------------------
// Macro definition for very fast access
// Add a chip select if needed
//------------------------------------------------------------------------------
#define LCD_Data(data) LCD_DATA_BUS = data;  LCD_nWR = 0; LCD_nWR = 1;
#define LCD_Cmd(data) LCD_DATA_BUS = data; LCD_DnC = 0; LCD_nWR = 0; LCD_nWR = 1; LCD_DnC = 1;

/******************************************************************************/
/* FUNCTION : LCD_PowerOff                                                    */
/* INPUT		: -                                                               */
/* OUTPUT		:                                                                 */
/******************************************************************************/
/* COMMENTS : Turns off LCD and all I/O pins ...                              */
/*            Do not forget to power off the LCD !!!                          */
/******************************************************************************/
void LCD_PowerOff(void);

/******************************************************************************/
/* FUNCTION : LCD_Lowlevel_Init                                               */
/* INPUT		: -                                                               */
/* OUTPUT		:                                                                 */
/******************************************************************************/
/* COMMENTS : Initialize ports, reset and so ...                              */
/******************************************************************************/
void LCD_Lowlevel_Init(void);

/******************************************************************************/
/* FUNCTION : lcd_read                                                        */
/* INPUT		: posX the x position of pixel to read                            */
/*            posY the y position of pixel to read                            */
/* OUTPUT		: the color read                                                  */
/******************************************************************************/
/* COMMENTS : Reads a pixel from the selected lcd memory position             */
/******************************************************************************/
uint16_t LCD_Read(uint16_t posX, uint16_t posY);

