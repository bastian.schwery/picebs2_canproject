/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif
#define _XTAL_FREQ 64000000L

#include "picebs2_canlib/can.h"
#include "picebs2_lcdlib/lcd_highlevel.h"

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/


/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{
    /* Configure the oscillator for the device works @ 64MHz*/
   PLLEN = 1;            // activate PLL x4
   OSCCON = 0b01110000;  // for 64MHz cpu clock (default is 8MHz)
    // Caution -> the PLL needs up to 2 [ms] to start !
    __delay_ms(2); 
    /* Initialize I/O and Peripherals for application */


    while(1)
    {

    }

}

